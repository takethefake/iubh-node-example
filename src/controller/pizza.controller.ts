import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Pizza, pizzaSchema } from "../entity/pizza.model";

export const getPizza = async (_: Request, res: Response): Promise<void> => {
  const pizzaRepository = await getRepository(Pizza);

  const pizza = await pizzaRepository.find();
  res.send(pizza);
};

export const postPizza = async (req: Request, res: Response): Promise<void> => {
  const pizzaRepository = await getRepository(Pizza);

  try {
    const validatedBody = await pizzaSchema.validate(req.body);
    const pizza = Pizza.create(validatedBody);

    const countHasPizzaWithName = await pizzaRepository.count({
      name: pizza.name,
    });

    if (countHasPizzaWithName > 0) {
      throw new Error(`There is already a Pizza with the name "${pizza.name}"`);
    }

    const savedPizza = await pizzaRepository.save(pizza);
    res.send(savedPizza);
  } catch (e) {
    if (e.name === "ValidationError" && e.errors) {
      res.send(e.errors);
    } else {
      res.send({ error: e.message });
    }
  }
};
