import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";

import { PizzaBestellung } from "./pizzaBestellung.model";
import * as yup from "yup";

export const bestellungSchema = yup.object().shape({
  kundennummer: yup.string().required(),
  items: yup
    .array()
    .of(
      yup.object().shape({
        anzahl: yup.number().required(),
        id: yup.string().uuid().required(),
      })
    )
    .required(),
});

@Entity()
export class Bestellung {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column()
  kundennummer!: string;

  @CreateDateColumn()
  createdDate!: Date;

  @OneToMany(
    () => PizzaBestellung,
    (pizzaBestellung) => pizzaBestellung.bestellung
  )
  pizzaBestellungen!: PizzaBestellung[];
}
