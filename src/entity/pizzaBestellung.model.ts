import { Column, Entity, ManyToOne } from "typeorm";
import { Bestellung } from "./bestellung.model";
import { Pizza } from "./pizza.model";

@Entity()
export class PizzaBestellung {
  @Column()
  anzahl!: number;

  @ManyToOne(() => Pizza, (pizza) => pizza.pizzaBestellungen, { primary: true })
  pizza!: Pizza;

  @ManyToOne(() => Bestellung, (bestellung) => bestellung.pizzaBestellungen, {
    primary: true,
  })
  bestellung!: Bestellung;
}
