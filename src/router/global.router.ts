import { Router } from "express";
import { pizzaRouter } from "./pizza.router";
import { bestellungRouter } from "./bestellung.router";

export const globalRouter = Router({ mergeParams: true });

interface HelloWorldReponse {
  message: string;
}
globalRouter.get("/", (req, res) => {
  res.send({ message: "hello world global" } as HelloWorldReponse);
});

globalRouter.use("/pizza", pizzaRouter);
globalRouter.use("/bestellung", bestellungRouter);
