import { Router } from "express";
import {
  getBestellung,
  postBestellung,
} from "../controller/bestellung.controller";

export const bestellungRouter = Router({ mergeParams: true });

bestellungRouter.get("/", getBestellung);
bestellungRouter.post("/", postBestellung);
